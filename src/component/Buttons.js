import React, {Component} from 'react'
import { Text, TouchableOpacity, StyleSheet } from 'react-native'
import { PrimaryColor } from './ThemeColors'

import * as Animatable from 'react-native-animatable';

export default class Buttons extends Component {
    render(){
        const {text, onPress, animation, duration} = this.props
        return(
            <Animatable.View style={styles.signIn} animation={animation} duration={duration} >
                <TouchableOpacity onPress={onPress}>
                  <Text style={styles.signInText}>
                    {text}
                  </Text>
                </TouchableOpacity>
            </Animatable.View>
        )
    }
}

const styles = StyleSheet.create({
    signIn:{
        backgroundColor:PrimaryColor,
        marginTop:20,
        width:'35%',
        height:'7%',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:10
    },
    signInText:{
        fontWeight:'bold',
        fontSize:20
    },
})