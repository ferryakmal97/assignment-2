import React, {Component} from 'react';
import { View, StyleSheet, Image} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';

class SplashScreen extends Component {

    // storeData = async (value) => {
    //     try {
    //       await AsyncStorage.setItem('@Login', value)
    //     } catch (e) {
    //       // saving error
    //     }
    //   }

    // handleGetDataStorage = async () => {
    //     try {
    //         const value = await AsyncStorage.getItem('@Login')
    //         if(value !== null) {
    //             return this.props.navigation.replace('Home')
    //         }else{
    //             return this.props.navigation.replace('Login')
    //         }
    //     }catch (e) {
    //         alert(e)
    //     }
    // }

    componentDidMount() {
        setTimeout (()=>{
            if(this.props.isLogin){
                this.props.navigation.replace('Home')
            } else {
                this.props.navigation.replace('Login')
            }
           },2000
        )
    }
    
    render(){
        return(
            <View style={styles.container}>
                <Image source={require('../assets/salt.png')}/>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        isLogin: state.isLogin
    }
}

export default connect(mapStateToProps)(SplashScreen)

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#0b0b0b',
        alignItems: 'center',
        justifyContent:"center",
        flex:1
    },
})