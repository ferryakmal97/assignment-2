import React, {Component} from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { BackgroundColor, PrimaryColor } from '../component/ThemeColors'
import * as Animatable from 'react-native-animatable';

class HomeScreen extends Component{
    signOut = () => {
        this.props.setLogin(false)
        this.props.navigation.replace('Login')
    }
    
    render(){
        return (
            <View style={styles.container}>
              
                <Animatable.View style={styles.header} animation='fadeInDownBig' duration={2000}>
                    <View style={{justifyContent:'center', alignItems:'center'}}>
                        <Text style={{color:'#d8a31c'}}>Welcome!</Text>
                        <Text style={styles.textHeader}>Akmal Ghaffari</Text>
                        <Text style={styles.textHeader}>1712502085</Text>
                    </View>
                    <View>
                    <TouchableOpacity>
                        <Animatable.View style={styles.logOutButton}>
                            <Text style={{fontWeight:'bold'}} onPress={()=>this.signOut()}>LOGOUT</Text>
                        </Animatable.View>
                    </TouchableOpacity>
                    </View>
                </Animatable.View>
              
                <Animatable.View style={styles.box} animation='bounceIn' delay={1500} duration={2000}>
                    <View style={styles.boxComponent}>
                        <Text style={styles.title}>IPK</Text>
                        <Text style={styles.number}>3.26</Text>
                    </View>
                    <View style={styles.boxComponent}>
                        <Text style={styles.title}>SKS Lulus</Text>
                        <Text style={styles.number}>124</Text>
                    </View>
                    <View style={styles.boxComponent}>
                        <Text style={styles.title}>Total SKS</Text>
                        <Text style={styles.number}>130</Text>
                    </View>
                </Animatable.View>

                <Animatable.View style={styles.boxSchedule} animation='bounceInLeft' delay={1500} duration={2000} >
                    <Animatable.View animation='fadeIn' delay={2500} style={{justifyContent:'center', alignItems:'center'}} >
                        <Text style={styles.textSchedule}>Jadwal Hari Ini</Text>
                        <View style={styles.mataKuliah}>
                            <Text style={styles.textHeader}>A2</Text>
                            <Text style={styles.textHeader}>Statistik Ekonomi</Text>
                            <Text style={styles.textHeader}>18:31</Text>
                        </View>
                    </Animatable.View>
                </Animatable.View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return{
      users: state.users,
      isLogin: state.isLogin
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return{
      setLogin : (isLogin) => {
        dispatch({
          type: 'LOGOUT',
          payload: isLogin
        })
      }
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)

const styles = StyleSheet.create({
    container:{
      backgroundColor:'#284358',
      flex:1
    },
    header: {
        alignItems:'center',
        justifyContent:'center',
        paddingVertical:20,
        backgroundColor:BackgroundColor,
        elevation:10,
        width:'60%',
        alignSelf:'center',
        borderBottomLeftRadius:40,
        borderBottomRightRadius:40
    },
    textHeader: {
        color:PrimaryColor,
        fontSize:20
    },
    box: {
      backgroundColor:PrimaryColor,
      width:'90%',
      alignSelf:'center',
      elevation:5,
      borderRadius:10,
      flexDirection:'row',
      justifyContent:'center',
      marginTop:15
    },
    logOutButton: {
        backgroundColor:PrimaryColor,
        width:'70%',
        alignSelf:'center',
        elevation:5,
        borderRadius:8,
        flexDirection:'row',
        justifyContent:'center',
        marginTop:15,
        padding:3
    },
    boxComponent: {
      alignItems:'center',
      marginVertical:15,
      marginHorizontal:25
    },
    title:{
      fontSize:18
    },
    number:{
      fontSize: 22,
      fontWeight:'bold'
    },
    boxSchedule:{
      marginVertical:15,
      marginHorizontal:25,
      backgroundColor:BackgroundColor,
      borderRadius:10,
      width:'80%',
      alignSelf:'center',
      elevation:10,
      borderTopStartRadius:60,
      borderBottomEndRadius:60
    },
    textSchedule:{
      color:PrimaryColor,
      fontSize:22,
      marginTop:20
    },
    mataKuliah: {
      backgroundColor:'#232b2b',
      width:'80%',
      padding:10,
      marginBottom:30,
      borderRadius:10,
    }
})