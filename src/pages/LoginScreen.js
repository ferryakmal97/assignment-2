import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ToastAndroid} from 'react-native';
import InputText from '../component/InputText';
import Buttons from '../component/Buttons';
import {
  BackgroundColor,
  PrimaryColor,
  SecondaryColor,
} from '../component/ThemeColors';
import * as Animatable from 'react-native-animatable';
import {connect} from 'react-redux'

class LoginScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  toast(msg) {
    ToastAndroid.showWithGravity(msg, ToastAndroid.SHORT, ToastAndroid.CENTER);
  }

  handleButtonSignIn = () => {
    const {username, password} = this.state;
    const users = this.props.users.filter((item) => item.username === username);
    try {
      if (users[0].username === username) {
        if (users[0].password === password) {
          this.toast('Login Berhasil');
          this.props.setLogin(true)
          this.props.navigation.replace('Home');
          console.log(this.props.isLogin)
        } else {
          this.toast('Password Salah');
        }
      } else {
        this.toast('Username Salah');
      }
    } catch (error) {
      this.toast('Isi Username dan Password Terlebih Dahulu');
    }

    // console.log(this.props.users)
    // const {username, password} = this.state;
    // this.props.users.filter(user => user.username === username).map(filteredUser => {
    //   try {
    //     if (username === filteredUser.username) {
    //       if (password === filteredUser.password) {
    //         this.props.navigation.replace('Home');
    //       } else {
    //         alert('Password Salah');
    //       }
    //     } else {
    //       alert('Username Salah');
    //     }
    //   } catch (error) {
    //     alert('Data belum diinput')
    //   }
    // })
  }

  render() {
    return (
      <View style={styles.container}>

        <Animatable.Image
          source={require('../assets/salt.png')}
          style={styles.logoStyle}
          resizeMode='stretch'
          animation='fadeInDown'
          duration={2000}
        />
        <Animatable.View 
          style={styles.box} 
          animation='fadeInLeft'
          duration={1500}>
          <Animatable.View 
            animation={'fadeIn'}
            duration={1000}
            delay={1000}>
            <Text style={styles.textLogin}>Login</Text>

            <InputText
              iconName="user-alt"
              placeHolder="Username"
              onChangeText={(value) => this.setState({username: value})}
            />
            <InputText
              iconName="key"
              password="password"
              placeHolder="Password"
              onChangeText={(password) => this.setState({password})}
            />

            <View style={styles.textRegister}>
              <Text>Don't have an account? Sign Up</Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Register')}>
                <Text style={{color: '#d8a31c', fontWeight: 'bold'}}> Here</Text>
              </TouchableOpacity>
            </View>
          </Animatable.View>
        </Animatable.View>
        <Buttons 
          onPress={() => this.handleButtonSignIn()} 
          text="SIGN IN"
          animation={'fadeInUpBig'}
          duration={2000} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return{
    users: state.users,
    user: state.username,
    pass: state.password,
    email: state.email,
    fullName: state.fullName,
    isLogin: state.isLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    setLogin : (isLogin) => {
      dispatch({
        type: 'LOGIN',
        payload: isLogin
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

const styles = StyleSheet.create({
  container: {
    backgroundColor: BackgroundColor,
    alignItems: 'center',
    flex: 1,
  },
  box: {
    backgroundColor: SecondaryColor,
    width: '90%',
    borderRadius: 50,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  logoStyle: {
    margin: 30,
  },
  textLogin: {
    fontWeight: 'bold',
    fontSize: 25,
    alignSelf: 'center',
    color: PrimaryColor,
    marginBottom: 10,
  },
  textRegister: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
});
