import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ToastAndroid,
  ScrollView,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import Buttons from '../component/Buttons';
import InputText from '../component/InputText';
import {connect} from 'react-redux'
import { addData} from '../redux/action/Action';
import * as Animatable from 'react-native-animatable';

class RegisterScreen extends Component {
  constructor(){
    super()
    this.state = {
      fullName:'',
      username:'',
      password:'',
      email:'',
    }
  }

  toast(msg) {
    ToastAndroid.showWithGravity(msg, ToastAndroid.SHORT, ToastAndroid.CENTER);
  }


  _handleButtonSignUp = () => {
    const {fullName, username, password, email} = this.state
    this.props.addData({
      fullName, username, password, email
    })
    this.toast('Register Success');
    this.props.navigation.navigate('Login')
    console.log(this.props.users)
  };

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Animatable.Image resizeMode='stretch' animation='fadeInDownBig' duration={2000} source={require('../assets/salt.png')} style={styles.logoStyle}/>
          <Animatable.View style={styles.box} animation='fadeInLeft'
          duration={1500} >
            <Animatable.View animation='fadeIn' duration={1000} delay={1000}>
              <Text style={styles.textRegister}>Register</Text>

              <InputText iconName="user-alt" placeHolder="FullName" onChangeText={(e) => this.setState({fullName:e})} />

              <InputText iconName="user-alt" placeHolder="Username" onChangeText={(e) => this.setState({username:e})} />

              <InputText iconName="at" placeHolder="Email" onChangeText={(e) => this.setState({email:e})} />

              <InputText iconName="key" password="password" placeHolder="password" onChangeText={(e) => this.setState({password:e})} />

              <View style={styles.textLogin}>
                <Text>Already have an account? Sign In</Text>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Login')}>
                  <Text style={{color: '#d8a31c', fontWeight: 'bold'}}> Here</Text>
                </TouchableOpacity>
              </View>
            </Animatable.View>
          </Animatable.View>
          <Buttons 
            text="SIGN UP" 
            onPress={() => this._handleButtonSignUp()} 
            animation={'fadeInUpBig'}
            duration={2000} />
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  return{
    users: state.users,
    user: state.username,
    pass: state.password,
    email: state.email,
    fullName: state.fullName
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    addData : (data) => dispatch(addData(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);

const {height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#0b0b0b',
    alignItems: 'center',
    height: height,
  },
  box: {
    backgroundColor: '#f6f6f6',
    width: '90%',
    borderRadius: 50,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  logoStyle: {
    margin: 30,
  },
  textRegister: {
    fontWeight: 'bold',
    fontSize: 25,
    alignSelf: 'center',
    color: '#d8a31c',
    marginBottom: 10,
  },
  textLogin: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
});
