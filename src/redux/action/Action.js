const addData = (data) => {
    return dispatch => {
        dispatch({type: 'ADD-DATA', payload:data})
    }
}

export {addData}