import React, {Component} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from './pages/SplashScreen';
import LoginScreen from './pages/LoginScreen';
import RegisterScreen from './pages/RegisterScreen';
import HomeScreen from './pages/HomeScreen';

const Stack = createStackNavigator();

const hide = {headerShown: false};

class Router extends Component {
  render(){
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Splash">
          <Stack.Screen name="Splash" component={SplashScreen} options={hide}/>
          <Stack.Screen name="Login" component={LoginScreen} options={hide}/>
          <Stack.Screen name="Register" component={RegisterScreen} options={hide}/>
          <Stack.Screen name="Home" component={HomeScreen} options={{headerShown: true}}/>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default Router;