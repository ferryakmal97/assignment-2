import React, {Component} from 'react';
import {Provider} from 'react-redux'
import {Store, Persistor} from './src/redux/store'
import  Router from './src/Router'
import {PersistGate} from 'redux-persist/integration/react'

class App extends Component {
  render(){
    return (
      <Provider store={Store}>
        <PersistGate loading={null} persistor={Persistor}>
          <Router/>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;